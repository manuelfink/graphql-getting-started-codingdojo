# GraphQL Coding Dojo

  1. [Why we need GraphQL - A quick intro](katas/01-intro-why-we-need-graphql.md)
  2. [Prepare / Prerequisites](katas/02-prerequisites.md) 
  3. [Sample App Idea](katas/03-app-idea.md)
  4. [GraphQL hello world - first server + query ](katas/04-graphql-hello-world/README.md) 
     * [solution ](katas/04-graphql-hello-world/SOLUTION.md) 
  5. [GraphQL server - query schema and resolvers ](katas/05-graphql-server/README.md) 
     * [solution ](katas/05-graphql-server/SOLUTION.md)
  6. [GraphQL server mutations ](katas/06-mutations/README.md) 
     * [solution ](katas/06-mutations/SOLUTION.md)
  7. [GraphQL server filtering pagination and sorting](../07-query-filter/README.md)
     * [solution](../07-query-filter/SOLUTION.md)


  * [Collection of further katas / ideas without samples](katas/90-furthor-dojos.md)