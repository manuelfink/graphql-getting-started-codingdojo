# Further GraphQL Dojos

The following dojos have not yet been elaborated. Feel free to create further dojos and hand them in with pull request.

* **Serverless GraphQL:** Run the [GraphQL Server serverless on aws lambda architecture](https://www.apollographql.com/docs/apollo-server/servers/lambda.html) to ensure scalability

* **GraphQL "API Management":** Build a GraphQL Server which is taking over the typical api management functionalities like authorization, caching, monitoring, logging, error tracing.

 * **Schema Stitching:** Combining multiple GraphQL Servers in a single endpoint. Thinking in micro service architecture and domain driven design this is very handy. This is a kind of GraphQL proxy server.

 * **Resilience Design:** Thinking in large scale applications a resilient application design is crucial. Keeping the system healthy while single micro services or endpoints fail is elementary! Fail fast fail forward. Isolate latency and fault tolerance of edges eg. with [hystixjs](https://www.npmjs.com/package/hystrixjs)


* **SQL n+1 Challenge - DO NOT OPTIMIZE:** The API should maintain scalable. If requesting children request to DB are send for every row, requests fan out and increase exponentially. Make a connector which solves the n+1 challenge and avoids every request to fan out for joined request. Recommendations say don't optimize for joins, use caching and batching instead. This just ends in heavy cost for hardly maintainable code and most likely not higher code performance! [How to build a GraphQL Server](https://dev-blog.apollodata.com/how-to-build-graphql-servers-87587591ded5)
