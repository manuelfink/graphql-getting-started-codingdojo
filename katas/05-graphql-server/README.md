# Kata 5: First GraphQL Server - Extending the Schema 

This dojo's tasks are:

  * Defining the schema of your sample app
  * Getting the GraphQL server for your schema up and running by
    * Define schema
    * Define resolvers: get called by the apollo server schema and foreword request to business model.
    * Define a (business) model / domain: Fetches data using connectors
    * Define connectors: Feel free to store results wherever you like eg. in memory, sql db, memcache, mongo. Using SQL with [sequelize](http://docs.sequelizejs.com/) and sqlite may be fastest solution 
  * [Apollo Engine](https://www.apollographql.com/engine/) can be added for caching and tracing if you are fancy.

  --- 
 [<< PREVIOUS KATA](../04-graphql-hello-world/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../06-mutations/README.md)
 
 ---