import { Author, Post, View, FortuneCookie } from './connectors';

function AuthorsModel(){
    this.find = function(args = {}){
        return Author.find({ where: args });
    }
    this.findAll = function(args = {}){
        return Author.findAll({ where: args });
    }
}

function AuthorModel(author) {
    console.log("PostModel_constructed, post");
    this.author = author;
    
    this.getPosts = function () {
        return this.author ? this.author.getPosts() : null ;
    };

}


function PostsModel (){
    this.findAll = function(args = {}){
        return Post.findAll({ where: args });
    }
}

function PostModel(post) {
    console.log("PostModel_constructed, post");
    this.post = post;
    
    this.getAuthor = function () {
        return this.post ? this.post.getAuthor() : null ;
    };

    this.getViews = function() {
        return this.post ? this.post.views : null;
    };

    this.getVotes = function() {
        return this.post ? this.post.votes : null;
    };

    this.getComments = function() {
        return this.post ? this.post.getComments() : null;
    };

}

let FortuneCookieModel = FortuneCookie;

export { AuthorsModel, AuthorModel, PostModel, PostsModel, FortuneCookieModel };

