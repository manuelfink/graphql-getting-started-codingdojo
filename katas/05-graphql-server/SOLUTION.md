# Kata 5: Apollo Server

We changed a lot.

 1. server.js is now separated into schema, resolvers, models and connectors within the data folder.
 2. schema within [schema.js](data/schema.js) became a little bit more complex. Resolver for childrens where added.
 3. resolvers where added in [resolver.js](data/resolvers.js)
 4. A business logic layer was added in [models.js](data/models.js) which gets data through orm models within [connectors.js](data/connectors.js)
 5. Connectors in [connectors.js](data/connectors.js) for now just provide sequelize models and create a local sqlite db and fills it with some mock data through casual.js

Since a lot changed take a look into the code and follow the structure there.

## Query

start the server with `npm start` and browse to http://localhost:3000/graphiql


You now can query the posts through:

```
{
  allAuthors {
    firstName
    posts {
      id
      title
      views
    }
  }
}
```

```
{
  posts {
    id
    title
    author {
      id
      firstName
    }
    views
  }
}

```

```
{
  author(firstName: "Maurine") {
    id
    firstName
    lastName
    posts {
      id
      text
      title
    }
  }
}

```

--- 
 [<< PREVIOUS KATA](../04-graphql-hello-world/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../06-mutations/README.md)
 
---



### Addional notes

Server documentation and explanations can be found in the [Apollo Server docs](https://www.apollographql.com/docs/apollo-server/)

This sample is based on the apollo-server [apollo-tutorial kit](https://github.com/apollographql/apollo-tutorial-kit) - [How to build a GraphQL server](https://medium.com/apollo-stack/tutorial-building-a-graphql-server-cddaa023c035#.wy5h1htxs) 