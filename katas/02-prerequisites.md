# Kata 2: Prepare / Prerequisites 

The following requirements should be prepared / installed ahead of the coding dojo. Install:

* Install [NodeJS LTS (Latest stable version)](https://nodejs.org/en/download/)
* Install [GraphQL Playground](https://github.com/graphcool/graphql-playground/releases/)
* Code Editor for JS / TS / NodeJS of your choice eg. [Visual Studio Code](https://code.visualstudio.com/Download)
* [Checkout](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone) this git repository (`git clone https://bitbucket.org/manuelfink/graphql-getting-started-codingdojo`)
* Install depedencies with npm (run `npm install` in shell)

Further helpful:

* [GraphQL Schema Cheat Sheet](https://github.com/sogko/graphql-schema-language-cheat-sheet/raw/master/graphql-shorthand-notation-cheat-sheet.pdf)

--- 
 [<< PREVIOUS KATA](./01-intro-why-we-need-graphql.md) | [ NEXT KATA >>](./03-app-idea.md)
 
---