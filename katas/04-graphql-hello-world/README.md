# Kata 4: GraphQL Server Hello World

Set up and get the first server running and fire the first query:

 * Define a GraphQL schema with a single field returning a single string of data saying "Hello World!"
    * Mocks can be used for now instead of using a resolver!
 * Setup express server with
   * Route /graphql providing the graph server endpoint with the mock schema (hint, requires bodyParser.json to work)
   * Route /graphiql to server graphiqlExpress to provide a simple api to query

--- 
 [<< PREVIOUS KATA](../03-app-idea.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../05-graphql-server/README.md)
 
 ---

### Literature

* [Apollo Server docs](https://www.apollographql.com/docs/apollo-server/)

For fast forward, take a look at:

* [Tutorial: How to build a GraphQL server with SQL, MongoDB and REST](https://dev-blog.apollodata.com/tutorial-building-a-graphql-server-cddaa023c035)
* https://github.com/apollographql/apollo-tutorial-kit