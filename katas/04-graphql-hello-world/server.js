import express from 'express';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import bodyParser from 'body-parser';


/** Mocks for types */




/** Define GraphQL Schema */

import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';

const typeDefs = `
type Query {
  posts: [Post]
}

type Post {
  id: Int
  title: String
}

`;

var posts = [
  {id:1, title:"Hello World"},
  {id:2, title:"Hello World 2"},
  {id:3, title:"Hello World 3"}
];

const resolvers = {
  Query: {
    posts(_, args) {
      return posts;
    }
  }
};

const schema = makeExecutableSchema({ typeDefs, resolvers });

/*const mocks = {
  String: () => 'Hello World!'
};*/

//addMockFunctionsToSchema({ schema, mocks });




/** Start the GraphQL Server */

const GRAPHQL_PORT = 3000;

const graphQLServer = express();

graphQLServer.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
graphQLServer.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

graphQLServer.listen(GRAPHQL_PORT, () =>
  console.log(
    `GraphiQL is now running on http://localhost:${GRAPHQL_PORT}/graphiql`
  )
);
