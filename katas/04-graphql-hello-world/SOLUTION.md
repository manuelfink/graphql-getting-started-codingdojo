# Kata 4: GraphQL Server Hello World - Solution


# GraphQL Hello World

Getting started ist pretty simple. The following steps are necessary to get the server running:

  1. Define a GraphQL Schema
  2. Define mock data
  3. Start a express server with a route providing the graphql server and graphiql. Graphiq

you can see the full code within [server.js](server.js)


## Getting started up and running

To get the initial server up and running, clone the repository to have a copy on your device. Go in the terminal into the folder, install dependencies and start the server.

```bash
git clone https://github.com/manuelfink/graphql-getting-started-codingdojo/
cd graphql-getting-started-codingdojo/dojos/04-graphql-hello-world/
npm install
npm start
```

The `npm start` command is configured within [package.json](package.json) to run with nodemon. Nodemon automatically restarts the node.js server on code changes and error.

## Running your first query

Then open [http://localhost:3000/graphiql](http://localhost:3000/graphiql)

When you paste this on the left side of the page:

```graphql
{
  testString
}
```

and hit the play button (cmd-return), then you should get this on the right side:

```json
{
  "data": {
    "testString": "It works!"
  }
}
```
--- 
 [<< PREVIOUS KATA](../03-app-idea.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../05-graphql-server/README.md)
 
 ---

### Addional notes

Server documentation and explanations can be found in the [Apollo Server docs](https://www.apollographql.com/docs/apollo-server/)

This sample is based on the apollo-server [apollo-tutorial kit](https://github.com/apollographql/apollo-tutorial-kit). You find a full getting started tutorial with sql, mongo and rest on [How to build a GraphQL server](https://medium.com/apollo-stack/tutorial-building-a-graphql-server-cddaa023c035#.wy5h1htxs) and its [server-tutorial-solution](https://github.com/apollographql/apollo-tutorial-kit/tree/server-tutorial-solution) branch.