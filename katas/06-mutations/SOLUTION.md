# Kata 6: Adding Mutations


The following files have been updates:
 * schema.js
 * resolver.js
 * model.js

[schema.js](data/schema.js) we added the root mutations

```graphql
type Mutation {
  postSave( id: Int, title: String, text: String): Post
  postUpvote ( id: Int): Post
  commentSave( id: Int, postId: Int, text: String): Comment
  commentUpvote ( id: Int): Comment
}
```

[resolver.js](data/resolvers.js) resolver for Mutations were added

```js
 Mutation: {
    postSave(_, args) {
      return (new PostModel(args)).setSave(args);
    },
    postUpvote(_, args) {
      return (new PostModel(args)).setUpvote(args);
    },
    commentSave(_, args) {
      return (new CommentModel(args)).setSave(args);
    },
    commentUpvote(_, args) {
      return (new CommentModel(args)).setUpvote(args);
    }
```

and finally also business logic setter within [models.js](data/models.js) are added to write changed data to the database


--- 
 [<< PREVIOUS KATA](../05-graphql-server/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../07-query-filter/README.md)
 
---