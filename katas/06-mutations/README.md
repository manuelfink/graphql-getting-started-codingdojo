# Kata 6: Adding Mutations

This kata's tasks is:

  * Add the mutations to your application
    * Insert Items
    * Update Items
    * Do some upvoting
  * Hints
    * GraphQL mutations work like queries
    * Sequelize has a findOrBuild method, see [Sequelize Models](http://docs.sequelizejs.com/class/lib/model.js~Model.html)
    * Consider uuid's instead of id's. Client generated UUID's can avoid multiple insert on poor network connections.   

--- 
 [<< PREVIOUS KATA](../05-graphql-server/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../07-query-filter/README.md)
 
---