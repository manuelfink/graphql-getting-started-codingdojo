import { PostModel, PostsModel, AuthorsModel, AuthorModel, CommentModel, FortuneCookieModel} from './models';

const resolvers = {
  Query: {
    author(_, args) {
      return (new AuthorsModel()).find(args);
    },
    posts(_, args) {
      return (new PostsModel()).findAll(args);
    },
    allAuthors(_, args) {
      return (new AuthorsModel()).findAll(args);
    },
    getFortuneCookie() {
      return FortuneCookieModel.getOne();
    }
  },
  Author: {
    posts(author) {
      return (new AuthorModel(author)).getPosts();
    }
  },
  Post: {
    author(post, args) {
      return (new PostModel(post)).getAuthor();
    },
    views(post, args) {
      return (new PostModel(post)).getViews();
    },
    comments(post, args) {
      return (new PostModel(post)).getComments();
    }
  },
  Mutation: {
    postSave(_, args) {
      return (new PostModel(args)).setSave(args);
    },
    postUpvote(_, args) {
      return (new PostModel(args)).setUpvote(args);
    },
    commentSave(_, args) {
      return (new CommentModel(args)).setSave(args);
    },
    commentUpvote(_, args) {
      return (new CommentModel(args)).setUpvote(args);
    }
  }
};

export default resolvers;
