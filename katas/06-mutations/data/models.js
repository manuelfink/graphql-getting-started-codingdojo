import { Author, Post, View, Comment, FortuneCookie } from './connectors';
import { create } from 'domain';

function AuthorsModel(){
    this.find = function(args = {}){
        return Author.find({ where: args });
    }
    this.findAll = function(args = {}){
        return Author.findAll({ where: args });
    }
}

function AuthorModel(author) {
    console.log("PostModel_constructed, post");
    this.author = author;
    
    this.getPosts = function () {
        return this.author ? this.author.getPosts() : null ;
    };

}


function PostsModel (){
    this.findAll = function(args = {}){
        return Post.findAll({ where: args });
    }
}

function PostModel(post) {
    console.log("PostModel_constructed, post");
    this.post = post;
    
    this.getAuthor = function () {
        return this.post ? this.post.getAuthor() : null ;
    };

    this.getViews = function() {
        return this.post ? this.post.views : null;
    };

    this.getVotes = function() {
        return this.post ? this.post.votes : null;
    };

    this.getComments = function() {
        return this.post ? this.post.getComments() : null;
    };

    this.setSave = function (data) {
        let id = this.post ? this.post.id : null || data ? data.id : null || null;
        //console.log("PostModel.setSave", id);
        return Post.findOrBuild({where:{id}})
            .then((r, created) => {
                //console.log("PostModel.then", r, created);
                if (!r) return {};
                post = r[0];
                
                if(data.title) post.title = data.title;
                if(data.text) post.text = data.text;
                post.votes = post.votes || 0;

                return post.save();
            })
    }

    this.setUpvote = function (data) {
        let id = this.post ? this.post.id : null || data ? data.id : null || null;
        return Post.find({where:{id}})
            .then((post) => {
                console.log("PostModel.then", post);
                if (!post) return {};
                post.votes = (post.votes || 0) + 1;
                return post.save();
            })
    }
}

function CommentModel(comment) {

    this.comment = comment;

    this.setSave = function (data) {
        let id = this.comment ? this.comment.id : null || data ? data.id : null || null;
        console.log("CommentModel.setSave", id);
        return Comment.findOrBuild({where:{id}})
            .then((r, created) => {
                console.log("CommentModel.then", r, created);
                if (!r) return {};
                comment = r[0];
                
                if(data.text) comment.text = data.text;
                comment.postId = comment.postId || data.postId;
                comment.votes = comment.votes || 0;

                return comment.save();
            })
    }

    this.setUpvote = function (data) {
        let id = this.comment ? this.comment.id : null || data ? data.id : null || null;
        return Comment.find({where:{id}})
            .then((comment) => {
                console.log("CommentModel.then", comment);
                if (!comment) return {};
                comment.votes = (comment.votes || 0) + 1;
                return comment.save();
            })
    }
}

let FortuneCookieModel = FortuneCookie;

export { AuthorsModel, AuthorModel, PostModel, PostsModel, CommentModel, FortuneCookieModel };

