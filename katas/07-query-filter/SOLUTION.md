# Kata 7: Adding filtering pagination and sorting to GraphQL server

Adding filtering, pagination and sorting two things there done:

1. Adding query parameters to [schema](data/schema.js) of posts:
    * first: to limit the number of returned records. Add a default and a max value for security!
    * offset: to get the next elements
    * after: to get the elements after a specific record
    * title: to filter on title content. Here sql like is used.
2. Business logic in [models.js](data/models.js) for filtering is added to posts
    * In a real application you may consider adding this to an default library.


[schema.js](data/schema.js) root query was changed to:

```graphql
posts(first: Int, offset:Int, after:Int, title: String)
```

[models.js](data/models.js) the logic for filtering was added to PostsModel

```js
function PostsModel (){
    this.findAll = function(args = {}){
        // Pagination, filtering, sorting added
        var limit = args.first || null;
        var offset = args.offset || 0;
        var order = [
            ['id', 'DESC'],
        ];
        var where = {};
        if(args.title) where.title = {$like:"%"+args.title+"%"};
        if(args.after) where.id = {$lt:args.after};
        
        return Post.findAll({ where, offset, limit, order });
    }
}
```

## Querying

By now you should know how to start the server. You can use the query parameters now like:

```graphql
{
  posts(first:2, after:5) {
    id
    title
  }
}

```

--- 
 [<< PREVIOUS KATA](../06-mutations/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../08-subscriptions/README.md)
 
---