# Kata 7: Adding filters, pagination and sorting to queries

This kata's tasks is:

  * Lets add some filtering
    * Filters adding search strings for specific fields
    * Paginating
    * Sorting
  * Hints
    * Filter parameters are submitted just like values for mutations
    * Lets just do this for one model eg. posts.
    * Pagination can be achieved through several ways
    * The [basics of pagination are well explained on graphql.org](http://graphql.org/learn/pagination/) 
    * Where apollo-server rather individual decides what each edge needs [GraphCOOL](https://blog.graph.cool/designing-powerful-apis-with-graphql-query-parameters-8c44a04658a9) uses scaffolding.

--- 
 [<< PREVIOUS KATA](../06-mutations/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../08-subscriptions/README.md)
 
---