import { PostModel, PostsModel, AuthorsModel, AuthorModel, CommentModel, FortuneCookieModel} from './models'; 

import { PubSub, withFilter } from 'graphql-subscriptions';

import { pubsub } from './subscriptions';

const resolvers = {
  Query: {
    author(_, args) {
      return (new AuthorsModel()).find(args);
    },
    posts(_, args) {
      return (new PostsModel()).findAll(args);
    },
    authors(_, args) {
      return (new AuthorsModel()).findAll(args);
    },
    getFortuneCookie() {
      return FortuneCookieModel.getOne();
    }
  },
  Author: {
    posts(author) {
      return (new AuthorModel(author)).getPosts();
    }
  },
  Post: {
    author(post, args) {
      return (new PostModel(post)).getAuthor();
    },
    views(post, args) {
      return (new PostModel(post)).getViews();
    },
    comments(post, args) {
      return (new PostModel(post)).getComments();
    }
  },
  Mutation: {
    postSave(_, args) {
      return (new PostModel(args)).setSave(args);
    },
    postUpvote(_, args) {
      return (new PostModel(args)).setUpvote(args);
    },
    commentSave(_, args) {
      return (new CommentModel(args)).setSave(args);
    },
    commentUpvote(_, args) {
      return (new CommentModel(args)).setUpvote(args);
    }
  },
  Subscription: {
    postUpdates: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("postUpdates"),
        (payload, variables) => {
          console.log("Subscription.postUpdates", payload, variables);
          return true;
        }
      )
    },
    commentUpdates: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("commentUpdates"),
        (payload, variables) => {
          console.log("Subscription.commentUpdates", payload, variables);
          return payload.comment.postId === variables.postId;
        }
      )
    }
  }
};

export default resolvers;
