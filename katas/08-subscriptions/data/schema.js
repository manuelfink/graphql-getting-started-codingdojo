import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
// import mocks from './mocks';
import resolvers from './resolvers';

const typeDefs = `
type Query {
  author(id: String, firstName: String, lastName: String): Author
  authors: [Author]
  posts(first: Int, offset:Int, after:Int, title: String): [Post]
  getFortuneCookie: String @cacheControl(maxAge: 5000)
}

type Author {
  id: Int
  firstName: String
  lastName: String
  posts: [Post]
}

type Post {
  id: Int
  title: String
  text: String
  views: Int
  votes: Int
  author: Author
  comments: [Comment]
}

type Comment {
  id: Int
  text: String
  votes: Int
}

type Mutation {
  postSave( id: Int, authorId: Int, title: String, text: String): Post
  postUpvote ( id: Int): Post
  commentSave( id: Int, postId: Int, text: String): Comment
  commentUpvote ( id: Int): Comment
}

type Subscription {
  postUpdates: Post
  commentUpdates(postId: Int): Post
}
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

// addMockFunctionsToSchema({ schema, mocks });

export default schema;
