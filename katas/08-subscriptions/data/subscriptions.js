
import { PubSub, withFilter } from 'graphql-subscriptions';

console.log("PubSub.initialized"); // This should only happens once! Singleton!

const pubsub = new PubSub();

pubsub.subscribe("postUpdates",(args) => {
    console.log("postUpdates.emitted", args);
})


pubsub.subscribe("commentUpdates",(args) => {
    console.log("commentUpdates.emitted", args);
})

export {pubsub};