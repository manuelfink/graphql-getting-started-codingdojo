# Kata 8: GraphQL server subscriptions

1. Adding libraries for publish and subscripe
2. Creating subscription handler pubsub
3. Adding subscriptions to schema
4. Publishing on post and comment changes

We installed the following packages through:

```shell
npm install --save graphql-subscriptions subscriptions-transport-ws http
```

Adding subscription handler in [subscriptions.js](data/subscriptions.js). Important here is that this must be a singleton!

```js
import { PubSub, withFilter } from 'graphql-subscriptions';

console.log("PubSub.initialized"); // This should only happens once! Singleton!
const pubsub = new PubSub();

export {pubsub};
```

Publishing/emitting events on changes in [models.js](data/models.js)

```js
...
.then((post) => {
    // Kata 8: Publish events for subscriptions 
    console.log("Pub.postUpdates", post.id);
    pubsub.publish('postUpdates', {post: post.toJSON()}) 
    return post; 
});
...
```

--- 
 [<< PREVIOUS KATA](../05-graphql-server/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../07-query-filter/README.md)
 
---