# Kata 8: GraphQL server subscriptions

This kata's tasks is:

  * Adding subscriptions
    * Identify items / edges / values which are worth subscribing
    * Add a PubSub handler
    * Publish events on changes
    * Add websocket support to the GraphQL server
    * Add subscribers to GraphQL
  * Hints
    * Google is your friend ;-) there is a good tutorial for getting started with [GraphQL Subscriptions and apollo-server](https://dev-blog.apollodata.com/tutorial-graphql-subscriptions-server-side-e51c32dc2951)
    * Http request are short lived, you need some long living connection for PubSub functionality!
    * On a scalable environment you have to scale subscriptions endpoints. Eg. hosting on AWS consider using IoT Sockets, PubSub or AppSyncs build in functionality 
    * https://www.apollographql.com/docs/graphql-subscriptions/setup.html

--- 
 [<< PREVIOUS KATA](../05-graphql-server/README.md) | [KATA](./README.md) | [SOULUTION](./SOLUTION.md) | [ NEXT KATA >>](../07-query-filter/README.md)
 
---