# Kata 3: GraphQL Sample App Idea

Select an simple app idea, which you want to implement with this coding dojo. Something like:

* Chat App
* Task App
* Discussion / news comment system
* Real time dashboard

We recommend that your idea has the following requirements:

* Multiple fields, which are not always needed. Avoiding over and under fetching. Eg. having some kind of a overview list, where not all information is needed.
* Relations eg. posts with comments and authors
* Real time updates: Eg. new messages, upvotes, ...
* Nice-to-have: Available subscribable real time feed eg. [twitter streaming api](https://brightplanet.com/2013/06/twitter-firehose-vs-twitter-api-whats-the-difference-and-why-should-you-care/) or [slack rtm](https://api.slack.com/rtm). We will connect to it as an external data source.

--- 
 [<< PREVIOUS KATA](./02-prerequisites.md) | [ NEXT KATA >>](./04-graphql-hello-world/README.md)
 
---


### My sample

I will build a simple thread based chat app

To give it a some frame it will:

 * have several threads
 * threads and messages can be upvoted and downvoted
 * users can chat within threads

