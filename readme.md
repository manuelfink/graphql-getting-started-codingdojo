# GraphQL Getting Started - Coding Dojo

"Learning by Doing" - This GraphQL getting started tutorial is designed as a coding dojo. You will be learn the essentials step by step by building a GraphQL server yourself with small exercises. 

## GraphQL Advantages

We will develop a small app showing the superior advantages over alternatives like REST, SOA and ad-hoc webservice architectures

* Avoid over-fetching as well as under-fetching
* Pub/sub realtime reactive updates through subscriptions
* Offline support and caching




## Agenda / Content

  1. [Why we need GraphQL - A quick intro](katas/01-intro-why-we-need-graphql.md)
  2. [Prepare / Prerequisites](katas/02-prerequisites.md) 
  3. [Sample App Idea](katas/03-app-idea.md)
  4. [GraphQL hello world - first server + query ](katas/04-graphql-hello-world/README.md) 
     * [solution ](katas/04-graphql-hello-world/SOLUTION.md) 
  5. [GraphQL server - query schema and resolvers ](katas/05-graphql-server/README.md) 
     * [solution ](katas/05-graphql-server/SOLUTION.md)
  6. [GraphQL server mutations ](katas/06-mutations/README.md) 
     * [solution ](katas/06-mutations/SOLUTION.md)
  7. [GraphQL server filtering pagination and sorting](../07-query-filter/README.md)
     * [solution](../07-query-filter/SOLUTION.md)
  8. [GraphQL server subscriptions](../08-subscriptions/README.md)
     * [solution](../08-subscriptions/SOLUTION.md)


  * [Collection of further katas / ideas without samples](katas/90-furthor-dojos.md)

## Literature recommendations

  * [GraphQL.org specification, learn GraphQL, queries, best  ](http://graphql.org/learn/queries/)
  * [HowToGraphQL.com](https://www.howtographql.com/)
  * [Apollo Server docs](https://www.apollographql.com/docs/apollo-server/)
  * [Apollo Client docs](https://www.apollographql.com/client/) available for [Angular](https://www.apollographql.com/docs/angular/), [React](https://www.apollographql.com/docs/react/), [Vue](https://github.com/akryum/vue-apollo), native apps and more.
  * [GraphQL Schema Cheat Sheet](https://github.com/sogko/graphql-schema-language-cheat-sheet/raw/master/graphql-shorthand-notation-cheat-sheet.pdf)
  * [Apollo Tutorial Kit](https://github.com/apollographql/apollo-tutorial-kit)

## Used Technology and Frameworks

This coding dojo is based on:

 * [apollographql/apollo-server](https://github.com/apollographql/apollo-server)


Today there is a wide range of GraphQL servers, clients, boilerplates, managed graphql servers and Backend-as-a-Service implementations available like [GraphCool Framework](https://github.com/graphcool/graphcool-framework), [Prisma](https://www.prismagraphql.com/), [AWS AppSync](https://aws.amazon.com/de/appsync/), [GraphQL YOGA](https://github.com/graphcool/graphql-yoga), which may suite specific task better, provide faster scaffolding or give opinionated implementations. See intro presentation for more details!


## Credits

This GraphQL Coding Dojo was initially created by [Manuel Fink](http://www.manuelfink.de) ( [LinkedIn](https://www.linkedin.com/in/manuelfink/) | [Git](https://github.com/manuelfink) ) as part of the [OC | Expert Camp Munich - Munich Coding Dojo Series](https://www.meetup.com/de-DE/Munchen-Coding-Dojo-Meetup/) by [OPITZ CONSULTING Deutschland GmbH](https://www.opitz-consulting.com). 

